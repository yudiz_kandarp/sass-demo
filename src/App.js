/* eslint-disable no-undef */
import React from 'react'
import { useEffect, useState } from 'react'
import Card from './components/card/Card'

import './App.scss'
function App() {
	const [data, setData] = useState()
	const [loading, setLoading] = useState(true)
	useEffect(() => {
		fetchData()
	}, [])

	async function fetchData() {
		try {
			const response = await fetch(
				`https://gateway.marvel.com/v1/public/characters?ts=1&apikey=${process.env.REACT_APP_API_KEY}&hash=${process.env.REACT_APP_API_HASH}`
			)
			const result = await response.json()
			console.log(result.data.results)
			setData(result.data.results)
			setLoading(false)
		} catch (error) {
			console.log(error)
		}
	}

	return (
		<>
			<h1 className='title'> welcome to marvel studio</h1>
			{loading && <h3 className='title'>Loading...</h3>}
			<div className='App'>
				{data?.map((item) => (
					<Card key={item.id} item={item} />
				))}
			</div>
		</>
	)
}

export default App
