import React from 'react'
import PropTypes from 'prop-types'
import './Card.scss'

function Card({ item }) {
	return (
		<div className='card'>
			<img
				className='main_image'
				src={item.thumbnail.path + '.' + item.thumbnail.extension}
				alt={item.name}
			/>
			<div className='details'>
				<h2 className='name'>{item.name} </h2>
				<div className='description'>
					{item.description || <h3>no description</h3>}{' '}
				</div>
			</div>
		</div>
	)
}
Card.propTypes = {
	item: PropTypes.object,
}
export default Card
